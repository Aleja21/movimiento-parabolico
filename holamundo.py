
from math import *
from time import sleep
from graphics import *

vo=60
t=0
wo=45*3.1415926535/180

y=0
g=9.8

ventana=GraphWin("simulador", 500,500)
ventana.setCoords(0,0,400,400)

while(y>=0):
    x=vo*cos(wo)*t
    y=vo*sin(wo)*t-(1.0/2)*g*t*t
    miCirculo=Circle(Point(x,y),10)
    miCirculo.draw(ventana)
    miCirculo.setFill('violet')
    print(x,y)
    sleep(0.1)
    t=t+0.4

ventana.getMouse()
